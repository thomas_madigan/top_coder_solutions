/*
This program solves the topcoder problem AdditionCycles
The class takes the first digit of a number and adds it
to the second number. It then combines the second digits
of the original number and the sum. This cycle continues
until  the second sum is the same as the original number.
The program returns the number of cycles.
*/
public class AdditionCycles
{
    public static int cycleLength(int x)
    {
        int num=x;
        num=addNum(num);
        int count=1;
        while(num!=x)
        {
            num=addNum(num);
            ++count;
        }
        return count;   
    }
    public static int addNum(int x)
    {
        int num1=(x/10)%10;
        int num2=(x%10);
        int sum=num1+num2;
		int sumNum2=sum%10;
        int result=Integer.parseInt(Integer.toString(num2)+Integer.toString(sumNum2));
     	return result;   
    }
}