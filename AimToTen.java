/*
The goal of this program is to find how many assignments are needed to get the average score to above 9.5 assuming that all
of the next assignments are scores of 10
*/
import java.util.*;
public class AimToTen
{
    //Finds the average of the vector passed to it named v
    public static double findVAve(Vector<Integer> v)
    {
     	double sum=0;
        for(int i=0; i<v.size();++i)
            sum+=v.get(i);
        return sum/v.size();
    }
	public static int need(int[] arr)
    {
        Vector<Integer> v=new Vector<Integer>();
        for(int i=0; i<arr.length;++i)
            v.add(arr[i]);
        while(true)
        {
        	double ave=findVAve(v);
        	if(ave>=9.5) break;
            else v.add(10);
        }
        return v.size()-arr.length;
    }
}